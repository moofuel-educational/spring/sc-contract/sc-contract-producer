<div align="center"><H1>Проект "Поставщик"</H1></div>
**Реализует сторону поставщика в демо-проекте, показывающем работу с CDC("Consumer Driven Contracts")**

Сторона потребителя: https://gitlab.com/moofuel-educational/spring/sc-contract/sc-contract-consumer

## Основные внешние зависимости:
#### 1. Java - 1.8
#### 2. Maven - 3.5.0
#### 3. Docker - 17.12.0-ce

## Тестирование
##### Для запуска CDC-тестов, находясь в корневой директории выполните следующую команду:
~~~
mvn clean test
~~~
##### Для генерации jar-файла с заглушками для потребителя выполните следующую команду:
~~~
mvn clean install
~~~

## Материалы по теме
* <a href="https://cloud.spring.io/spring-cloud-contract/">Spring Cloud Contract </a>
* <a href="https://martinfowler.com/articles/consumerDrivenContracts.html">Consumer-Driven Contracts: A Service Evolution Pattern</a>
* <a href="http://www.servicedesignpatterns.com/WebServiceEvolution/ConsumerDrivenContracts">Consumer-Driven Contracts design pattern</a>
* <a href="https://habrahabr.ru/company/avito/blog/333644/">Ядро автоматизации тестирования в микросервисной архитектуре</a>
* <a href="https://www.youtube.com/watch?v=MDydAqL4mYE">Видео: Consumer Driven Contracts and Your Microservice Architecture by Marcin Grzejszczak and Josh Long</a>
* <a href="https://www.youtube.com/watch?v=JEmpIDiX7LU">Видео: Consumer Driven Contracts and Your Microservice Architecture - Marcin Grzejszczak, Adib Saikali</a>
* <a href="https://github.com/pact-foundation/pact-js">Pact JS</a>
* <a href="https://medium.com/techbeatscorner/consumer-driven-contracts-with-pact-js-d45988f26e32">Consumer-Driven Contracts with Pact-JS</a>