package com.moofuel.demo.sccontractproducer.repository;

import com.moofuel.demo.sccontractproducer.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {
}
