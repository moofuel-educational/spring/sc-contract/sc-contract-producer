package com.moofuel.demo.sccontractproducer.service.impl;

import com.moofuel.demo.sccontractproducer.domain.Post;
import com.moofuel.demo.sccontractproducer.repository.PostRepository;
import com.moofuel.demo.sccontractproducer.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DefaultPostService implements PostService {
    private final PostRepository postRepository;

    @Override
    public Page<Post> findAll(Pageable pageable) {
        return this.postRepository.findAll(pageable);
    }

    @Override
    public Post findOne(Long id) {
        return this.postRepository
                .findById(id)
                .orElseThrow(IllegalStateException::new);
    }

    @Override
    public Post createNewPost(Post post) {
        return this.postRepository.save(post);
    }

    @Override
    public Post updateExisting(Long id, Post post) {
        final Post one = this.findOne(id);
        BeanUtils.copyProperties(post, one, "id", "date");
        return this.postRepository.save(one);
    }

    @Override
    public void deleteExisting(Long id) {
        this.postRepository.deleteById(id);
    }
}
