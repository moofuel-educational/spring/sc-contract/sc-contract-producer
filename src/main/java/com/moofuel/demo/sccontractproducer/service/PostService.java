package com.moofuel.demo.sccontractproducer.service;

import com.moofuel.demo.sccontractproducer.domain.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PostService {
    Page<Post> findAll(Pageable pageable);

    Post findOne(Long id);

    Post createNewPost(Post post);

    Post updateExisting(Long id, Post post);

    void deleteExisting(Long id);
}
