package com.moofuel.demo.sccontractproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScContractProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScContractProducerApplication.class, args);
    }
}
