package com.moofuel.demo.sccontractproducer.controller;

import com.moofuel.demo.sccontractproducer.domain.Post;
import com.moofuel.demo.sccontractproducer.service.PostService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

@RestController
@RequestMapping("/posts")
@RequiredArgsConstructor
@Slf4j
public class PostController {

    private final PostService postService;

    @GetMapping
    public Page<Post> findAll(Pageable pageable) {
        return this.postService.findAll(pageable);
    }

    @GetMapping("/{id}")
    private Post findOne(@PathVariable Long id) {
        return this.postService.findOne(id);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public IdDto createNew(@RequestBody Post post) {
        return new IdDto(this.postService.createNewPost(post).getId());
    }

    @PutMapping("/{id}")
    public Post updateExisting(@PathVariable Long id,
                               @RequestBody Post post) {
        return this.postService.updateExisting(id, post);
    }

    @DeleteMapping("/{id}")
    public void deleteExisting(@PathVariable Long id) {
        this.postService.deleteExisting(id);
    }

    @PostMapping("/{id}/pic")
    public Dto uploadPicToPost(@PathVariable Long id,
                                @RequestBody MultipartFile pic) throws IOException {
        final Path tempFile = Files.createTempFile(String.valueOf(id), "temp");
        try(final OutputStream tempFileOS = Files.newOutputStream(tempFile);
            final InputStream picInputStream = pic.getInputStream()) {
            StreamUtils.copy(picInputStream, tempFileOS);
        } catch (IOException e) {
            log.error("Error in processing file", e);
        }
        return new Dto(id, pic.getOriginalFilename());
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private class IdDto {
        private Long id;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private class Dto {
        private Long id;
        private String name;
    }
}
