package contracts

import org.springframework.cloud.contract.spec.Contract
import org.springframework.cloud.contract.spec.internal.ClientDslProperty

Contract.make {
    description 'Represents a deletion of post by its ID'
    request {
        method DELETE()
        ClientDslProperty number = anyNumber()
        url value(consumer("/posts/$number"), producer('/posts/1'))
    }
    response {
        status 200
    }
}
