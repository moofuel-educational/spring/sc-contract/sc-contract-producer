package contracts

import org.springframework.cloud.contract.spec.Contract

import java.util.regex.Pattern

Contract.make {
    description 'Represents an updating post by its id'
    request {
        method PUT()
        url value(consumer(regex('/posts/\\d+')), producer('/posts/1'))
        body(
                "date": value(consumer(regex("(\\d+){13}")), producer(new Date().getTime())),
                "title": $(anyNonBlankString()),
                "content": $(anyNonBlankString()),
                "author": $(anyNonBlankString())
        )
        headers {
            contentType(applicationJsonUtf8())
        }
    }

    response {
        status 200
        def javaDatePattern = Pattern.compile("([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.[0-9]{3})?(Z|[+-][01][0-9][0-5][0-9])")
        body(
                "id": $(fromRequest().path(1)),
                "date": value(javaDatePattern),
                "title": $(fromRequest().body('$.title')),
                "content": $(fromRequest().body('$.content')),
                "author": $(fromRequest().body('$.author'))
        )
        headers {
            contentType(applicationJsonUtf8())
        }
    }
}