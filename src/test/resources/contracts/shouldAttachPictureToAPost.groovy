package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Represents an attaching a picture to a post by its ID"
    request {
        method POST()
        url value(consumer("/posts/1/pic"), producer('/posts/1/pic'))
        multipart(
                pic: named(
                        name: value("filename1.jpg"),
                        content: value(consumer(regex(nonEmpty())), producer(anyNonBlankString()))
                )
        )
        headers {
            contentType(multipartFormData())
        }
    }

    response {
        status 200
        body(
                id: 1,
                name: value(producer("filename1.jpg"))
        )
        headers {
            contentType(applicationJsonUtf8())
        }
    }
}