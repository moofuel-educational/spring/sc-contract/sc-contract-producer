package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description("Represents a creation of a new post")
    request {
        method POST()
        url '/posts'
        body(
                "date" : value(consumer(regex("(\\d+){13}")), producer(new Date().getTime())),
                "title": $(anyNonBlankString()),
                "content": $(anyNonBlankString()),
                "author": $(anyNonBlankString())
        )
        headers {
            contentType(applicationJsonUtf8())
        }
    }

    response {
        status 201
        body(
                "id": $(anyNumber())
        )
        headers {
            contentType(applicationJsonUtf8())
        }
    }
}
