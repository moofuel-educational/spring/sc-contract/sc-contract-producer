package contracts

import org.springframework.cloud.contract.spec.Contract

import java.util.regex.Pattern

Contract.make {
    description 'Represents retrieving a page of posts'
    request {
        method GET()
        url '/posts'
    }

    def javaDate = Pattern.compile("([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.[0-9]{3})?(Z|[+-][01][0-9][0-5][0-9])")
    response {
        status 200
        body(
                "content": [
                        [
                                "id"     : $(anyNumber()),
                                "date"   : $(value(javaDate)),
                                "title"  : $(anyNonBlankString()),
                                "content": $(anyNonBlankString()),
                                "author" : $(anyNonBlankString())
                        ],
                        [
                                "id"     : $(anyNumber()),
                                "date"   : $(value(javaDate)),
                                "title"  : $(anyNonBlankString()),
                                "content": $(anyNonBlankString()),
                                "author" : $(anyNonBlankString())
                        ],
                        [
                                "id"     : $(anyNumber()),
                                "date"   : $(value(javaDate)),
                                "title"  : $(anyNonBlankString()),
                                "content": $(anyNonBlankString()),
                                "author" : $(anyNonBlankString())
                        ],
                        [
                                "id"     : $(anyNumber()),
                                "date"   : $(value(javaDate)),
                                "title"  : $(anyNonBlankString()),
                                "content": $(anyNonBlankString()),
                                "author" : $(anyNonBlankString())
                        ],
                        [
                                "id"     : $(anyNumber()),
                                "date"   : $(value(javaDate)),
                                "title"  : $(anyNonBlankString()),
                                "content": $(anyNonBlankString()),
                                "author" : $(anyNonBlankString())
                        ],
                        [
                                "id"     : $(anyNumber()),
                                "date"   : $(value(javaDate)),
                                "title"  : $(anyNonBlankString()),
                                "content": $(anyNonBlankString()),
                                "author" : $(anyNonBlankString())
                        ]
                ],
                "pageable": [
                        "sort"      : [
                                "sorted"  : $(value(Pattern.compile("true|false"))),
                                "unsorted": $(value(Pattern.compile("true|false")))
                        ],
                        "offset"    : $(anyNumber()),
                        "pageSize"  : $(anyNumber()),
                        "pageNumber": $(anyNumber()),
                        "unpaged"   : $(value(Pattern.compile("true|false"))),
                        "paged"     : $(value(Pattern.compile("true|false")))
                ],
                "last": $(value(Pattern.compile("true|false"))),
                "totalElements": $(anyNumber()),
                "totalPages": $(anyNumber()),
                "size": $(anyNumber()),
                "number": $(anyNumber()),
                "sort": [
                        "sorted"  : $(value(Pattern.compile("true|false"))),
                        "unsorted": $(value(Pattern.compile("true|false")))
                ],
                "numberOfElements": $(anyNumber()),
                "first": $(value(Pattern.compile("true|false")))
        )

        headers {
            contentType(applicationJsonUtf8())
        }
    }
}
