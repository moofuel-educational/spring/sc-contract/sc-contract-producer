package contracts

import org.springframework.cloud.contract.spec.Contract
import org.springframework.cloud.contract.spec.internal.ClientDslProperty

import java.util.regex.Pattern

Contract.make {
    description("Represents a retrieving post by its ID")
    request {
        method GET()
        ClientDslProperty number = anyNumber()
        url value(consumer("/posts/$number"), producer('/posts/1'))
    }

    def javaDate = Pattern.compile("([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\\.[0-9]{3})?(Z|[+-][01][0-9][0-5][0-9])")
    response {
        status 200
        body(
                "id": $(fromRequest().path(1)),
                "date": $(javaDate),
                "title": $(anyNonBlankString()),
                "content": $(anyNonBlankString()),
                "author": $(anyNonBlankString())
        )
        headers {
            contentType(applicationJsonUtf8())
        }
    }
}
