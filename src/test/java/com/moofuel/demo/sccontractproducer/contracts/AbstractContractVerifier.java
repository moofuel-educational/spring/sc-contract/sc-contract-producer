package com.moofuel.demo.sccontractproducer.contracts;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstractContainerizedTest.Config.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AbstractContractVerifier extends AbstractContainerizedTest {

    @LocalServerPort
    private int randomPort;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost:" + randomPort;
    }
}
