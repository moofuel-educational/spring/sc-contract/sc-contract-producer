package com.moofuel.demo.sccontractproducer.contracts;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.ClassRule;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.sql.DataSource;

/**
 * @author Дмитрий
 * @since 05.01.2018
 */
public abstract class AbstractContainerizedTest {


    @ClassRule
    public static PostgreSQLContainer container = new PostgreSQLContainer();

    @TestConfiguration
    public static class Config {

        @Bean
        public DataSource dataSource() {
            final HikariConfig hikariConfig = new HikariConfig();
            hikariConfig.setJdbcUrl(container.getJdbcUrl());
            hikariConfig.setDriverClassName(org.postgresql.Driver.class.getCanonicalName());
            hikariConfig.setUsername(container.getUsername());
            hikariConfig.setPassword(container.getPassword());
            return new HikariDataSource(hikariConfig);
        }
    }
}
